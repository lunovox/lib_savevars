![favicon]
# lib_savevars

[![minetest_icon]][minetest_link] Registers in file ````variables.tbl```` on LuaScript language the global variables and character variables that can be queried by other mods and by server administrator commands.

## 📦 **Dependencies:**

 * ---- There are no dependencies ----

## 📜 **License:**

 * [![license_icon]][license_link]

See more details in wiki: 
 
[![wiki_en_icon]][wiki_en_link] [![wiki_pt_icon]][wiki_pt_link]

## ️🔬 **Developers:**

 * Lunovox Heavenfinder: [email](mailto:lunovox@disroot.org), [social web](https://qoto.org/@lunovox), [WebChat](https://cloud.disroot.org/call/9aa2t7ib), [xmpp](xmpp:lunovox@disroot.org?join), [audio conference](mumble:mumble.disroot.org), [more contacts](https:libreplanet.org/wiki/User:Lunovox)

## 💾 **Downloads:**

* [Stable Versions] : There are several versions in different stages of completion.  Usually recommended for servers because they are more secure.

* [Git Repository] : It is the version currently under construction.  There may be some stability if used on servers.  It is only recommended for testers and mod developers.

## ️🌅 **Player information automatically saved in variable:**

 * ````last_ip```` : Last IP that the player had when connecting to the server.
 * ````listed_ip```` : All IPs that the player used to connect to the server.
 * ````last_login```` : Records the time the player last logged in the server in integer numerical format "os.time" of Luacript.
 * ````last_logout```` : Registers the moment the player last logged out on the server in integer numeric format "os.time" from Luacript.
 * ````time_played```` : Records the total time in seconds in the integer numeric format "os.time" of Luacript that the player has been logged into the server.
 
## 🌈 **Administrator Commands:**
 
 * ````/set_global_value <variable> <value>```` : Registers a global variable on the server.
 * ````/get_global_value <variable>```` : Queries a global variable on the server.
 * ````/del_global_value <variable>```` : Delete a global variable on the server.
 * ````/set_char_value <character/player> <variable> <value>```` : Registers a variable of a character/player on the server.
 * ````/get_char_value <character/player> <variable>```` : Queries a variable of a character/player on the server.
 * ````/del_char_value <character/player> <variable>```` : Delete a variable of a character/player on the server.
 
## ⚙️ **Settings:**

In **minetest.conf** file:

You don't need to do any of these presets mentioned below to make this mod work. But it's worth knowing the configuration values in case you want to directly change the ````minetest.conf```` file.

| Settings | Descryption |
| :-- | :-- |
| ````savevars.register_log = <boolean>````	| Allows you to print changes to variable values on the screen. Default: ````false````. | 
| ````savevars.debug = <boolean>````			| Allows you to print the debug information of this mod on the screen. Default: ````false````. | 

## ⚙️ **Methods to Mods:**

 * ````modsavevars.setGlobalValue("<variable>", <value>)```` : Registers a global variable on the server.
 * ````<table> modsavevars.getGlobalValue("<variable>")```` : Queries a global variable on the server.
 * ````modsavevars.setGlobalValue("<variable>", nil)```` : Delete a global variable on the server.
 * ````modsavevars.setCharValue("<character/player>", "<variable>", <value>)```` : Registers a variable of a character/player on the server.
 * ````<table> modsavevars.getCharValue("<character/player>", "<variable>")```` : Queries a variable of a character/player on the server.
 * ````modsavevars.setCharValue("<character/player>", "<variable>", nil)```` : Delete a variable of a character/player on the server.
 * ````<table> modsavevars.getAllDBChars()```` : Queries the database of all character/player on the server.
 * ````modsavevars.setAllDBChars(<table>)```` : Registers the database of all character/player on the server.
 * ````modsavevars.doOpen()```` : Loads the database into the server's RAM..
 * ````modsavevars.doSave()```` : Saves the database in file ````variables.tbl```` on LuaScript language.


[favicon]:https://gitlab.com/lunovox/lib_savevars/-/raw/master/favicon.png

[Git Repository]:https://gitlab.com/lunovox/lib_savevars
[Stable Versions]:https://gitlab.com/lunovox/lib_savevars/-/tags

[license_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=Download&color=yellow
[license_link]:https://gitlab.com/lunovox/lib_savevars/-/raw/master/LICENSE

[minetest_icon]:https://img.shields.io/static/v1?label=Minetest&message=Mod&color=brightgreen
[minetest_link]:https://minetest.net

[wiki_en_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=EN&color=blue
[wiki_en_link]:https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
[wiki_pt_icon]:https://img.shields.io/static/v1?label=GNU%20AGPL%20v3.0&message=PT&color=blue
[wiki_pt_link]:https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License


