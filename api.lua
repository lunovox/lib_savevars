minetest.register_privilege("savevars",  {
	description="Permite que o jogador edite variaveis por linha de comando.", 
	give_to_singleplayer=false,
})

function getBool(myVar)
    local res = minetest.settings:get(myVar)
    if (res ~= nil and (res == "1" or res == "true" or res == "TRUE")) then 
        return true;
    else
        return false;
    end
end

function setBool(myVar, myValue)
    if (type(myVar)=="string" and myVar~="") then
        if (type(myValue)=="boolean" and myValue==true) then
            minetest.settings:set(myVar, "TRUE")
        else
            minetest.settings:set(myVar, "FALSE")
        end
    else
		minetest.log('error',"[SAVEVARS:ERROR] Variável não declarada!")
    end
end

if not (getBool("savevars.register_log")~=true) then 
	setBool("savevars.register_log", false)
	core.setting_save() 
end

modsavevars = { }
modsavevars.fileVariables = minetest.get_worldpath().."/variables.tbl"
modsavevars.variables={  global={}, players={} 	}

modsavevars.debug = function(text)
	--print("getBool('savevars.debug') = "..dump(getBool("savevars.debug")))
	if getBool("savevars.debug") then
		minetest.chat_send_all("[SAVEVARS:DEBUG] "..text)
		minetest.log('warning',"[SAVEVARS:DEBUG] "..text) 
		--level is one of "none", "error", "warning", "action", "info", or "verbose". Default is "none".
	end
end

modsavevars.doPurgeOldChars = function()
	if minetest.player_exists then
		local tblPlayers = modsavevars.getAllDBChars()
		local tblPlayersNew = { }
		local numDeleted = 0
		local numPlayers = 0
		for playername, database in pairs(tblPlayers) do 
			if type(playername)=="string" and playername~="" then
				numPlayers = numPlayers + 1
				if minetest.player_exists(playername) then
					if type(database)~="nil" then
						tblPlayersNew[playername] = database
						--modsavevars.debug("[PURGE] tblPlayersNew['"..playername.."'] = database")
					else
						tblPlayersNew[playername] = { }
						--modsavevars.debug("[PURGE] tblPlayersNew['"..playername.."'] = nil")
					end
				else
					numDeleted = numDeleted + 1
				end
			end
		end
		if numDeleted > 0 then
			modsavevars.setAllDBChars(tblPlayersNew)
			minetest.log('warning',string.format("[SAVEVARS:PURGE] foram deletados %02d do total %02d jogadores do banco de dados do SAVEVARS.", numDeleted, numPlayers)) 
			modsavevars.doSave()
		end
	else
		minetest.log('erro',"[SAVEVARS:PURGE] function 'minetest.player_exists()' = [ERRO]") 
	end
	
	--]]
end

modsavevars.doOpen = function()
	--modsavevars.debug("modsavevars.fileVariables = "..modsavevars.fileVariables)
	local file = io.open(modsavevars.fileVariables, "r")
	if file then
		--modsavevars.debug("modsavevars.doOpen() : ok ")
		local table = minetest.deserialize(file:read("*all"))
		file:close()
		if type(table) == "table" then
			--modsavevars.debug("[modsavevars.doOpen()] type(table) == 'table' : ok ")
			modsavevars.variables = table
			return
		end
		minetest.log('action',"[SAVEVARS:OPEN] "..string.format("Opening database '%s'!", modsavevars.fileVariables))
	else
		minetest.log('warning',"[SAVEVARS:OPEN] "..string.format("Unable to open file '%s'!", modsavevars.fileVariables))
		minetest.log('warning',"[SAVEVARS:OPEN] Working with empty database!")
	end
end

modsavevars.doSave = function()
	--file = io.open(modsavevars.fileVariables,"a+")
	local file = io.open(modsavevars.fileVariables,"w")
	if file then
		file:write(minetest.serialize(modsavevars.variables))
		file:close()
	else
		minetest.log('error',"[SAVEVARS:ERROR] "..string.format("Unable to save file '%s'!", modsavevars.fileVariables))
	end
end

modsavevars.getAllDBChars = function() --Criado para o mod 'correio'
	return modsavevars.variables.players
end

modsavevars.setAllDBChars = function(tblPlayers) --Criado para o mod 'correio'
	modsavevars.variables.players = tblPlayers
end

modsavevars.setGlobalValue = function(variavel, valor)
	if modsavevars.variables.global==nil then
		modsavevars.variables.global = {}
	end
	if valor~=nil or modsavevars.variables.global[variavel]~=nil then --Verifica se nao ja estava apagada
		if type(valor)=="number" then valor = valor * 1 end --para transformar em numero
		modsavevars.variables.global[variavel] = valor
		if getBool("savevars.register_log") then
			if valor~= nil then
				minetest.log('action',"modsavevars.setGlobalValue("..variavel.."='"..dump(valor).."')")
			else
				minetest.log('action',"modsavevars.setGlobalValue("..variavel.."=nil)")
			end
		end
	end
	--modsavevars.doSave() --Desativado porque não é bom salvar sempre que uma variavel for alterada.
end
modsavevars.getGlobalValue = function(variavel)
	if modsavevars.variables.global~=nil and modsavevars.variables.global[variavel]~=nil then
		local valor = modsavevars.variables.global[variavel]
		if getBool("savevars.register_log") then
			minetest.log('action',"modsavevars.getGlobalValue("..variavel..") = '"..dump(valor).."'")
		end
		return valor
	end
	return nil
end
modsavevars.setCharValue = function(charName, variavel, valor)
	if modsavevars.variables.players[charName] == nil then
		modsavevars.variables.players[charName] = {}
	end
	if valor~=nil or modsavevars.variables.players[charName][variavel]~=nil then --Verifica se nao ja estava apagada
		modsavevars.variables.players[charName][variavel] = valor
		if getBool("savevars.register_log") then
			if valor~= nil then
				minetest.log('action',"modsavevars.setCharValue("..charName..":"..variavel.."='"..dump(valor).."')")
			else
				minetest.log('action',"modsavevars.setCharValue("..charName..":"..variavel.."=nil)")
			end
		end
	end
	--modsavevars.doSave() --Desativado porque não é bom salvar sempre que uma variavel for alterada.
end

modsavevars.getCharValue = function(charName, variavel)
	if modsavevars.variables.players[charName]~=nil and modsavevars.variables.players[charName][variavel]~=nil then
		local valor = modsavevars.variables.players[charName][variavel]
		if getBool("savevars.register_log") then
			minetest.log('action',"modsavevars.getCharValue("..charName..":"..variavel..") = '"..dump(valor).."'")
		end
		return valor
	end
	return nil
end

modsavevars.register_on_prejoinplayer = function(playername, ip)
	local listed_ip = modsavevars.getCharValue(playername, "listed_ip")
	if listed_ip==nil or listed_ip=="" then
		listed_ip = ip
	elseif not string.find(listed_ip, ip) then
		listed_ip = listed_ip .. ";" .. ip
	end
	local now = os.time()
	modsavevars.setCharValue(playername, "listed_ip", listed_ip)
	modsavevars.setCharValue(playername, "last_ip", ip)
	modsavevars.setCharValue(playername, "last_login", now)
	
	minetest.log('action',"[SAVEVARS:PREJOIN] The player '"..playername.."' prejoin now.")
	
	modsavevars.doSave()
end

modsavevars.register_on_leaveplayer = function(playername)
	local last_login = tonumber(modsavevars.getCharValue(playername, "last_login") or os.time())
	local last_logout = os.time()
	local time_played_old = tonumber(modsavevars.getCharValue(playername, "time_played") or 0)
	local time_played_now = last_logout - last_login
	local time_played_total = time_played_old + time_played_now

	minetest.log('action',"[SAVEVARS:LEAVE] "
		..string.format("The player '%s' did leave after %02d seconds of game, totaly %02d secounds.", playername, time_played_now, time_played_total)
	)
	
	modsavevars.setCharValue(playername, "last_logout", last_logout)
	modsavevars.setCharValue(playername, "time_played", time_played_total)
	modsavevars.doSave()
end

modsavevars.register_on_shutdown = function()
	local players_online = minetest.get_connected_players()
if players_online ~= nil then
for _,player in ipairs(players_online) do
		local playername = player:get_player_name()
		modsavevars.register_on_leaveplayer(playername)
	end
end
	modsavevars.doSave()
	minetest.log('action', "[SAVEVARS:SHUTDOWN] "..string.format("Saving all players database to '%s'!", modsavevars.fileVariables))
end

------------------------------------------------------------------------------------------------------------------------------

minetest.register_on_prejoinplayer(function(playername, ip)
	modsavevars.doPurgeOldChars()
	modsavevars.register_on_prejoinplayer(playername, ip)
end)

minetest.register_on_leaveplayer(function(player)
	modsavevars.register_on_leaveplayer(player:get_player_name())
end)

minetest.register_on_shutdown(function()
	modsavevars.register_on_shutdown()
end)

------------------------------------------------------------------------------------------------------------------------------

modsavevars.doOpen()


